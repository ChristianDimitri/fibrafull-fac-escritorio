﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFiltroGastos
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnImprimir = New System.Windows.Forms.Button()
        Me.VerAcceso2TableAdapter1 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.VerAcceso2TableAdapter2 = New softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnQuitarTodos = New System.Windows.Forms.Button()
        Me.btnQuitarUno = New System.Windows.Forms.Button()
        Me.btnAgregarTodos = New System.Windows.Forms.Button()
        Me.btnAgregarUno = New System.Windows.Forms.Button()
        Me.lbxAgregados = New System.Windows.Forms.ListBox()
        Me.lbxPorAgregar = New System.Windows.Forms.ListBox()
        Me.lblCajera = New System.Windows.Forms.Label()
        Me.cmbCajera = New System.Windows.Forms.ComboBox()
        Me.lblFechaInicial = New System.Windows.Forms.Label()
        Me.lblFechaFinal = New System.Windows.Forms.Label()
        Me.dtpFechaInicial = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaFinal = New System.Windows.Forms.DateTimePicker()
        Me.gbxFiltros = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout()
        Me.gbxFiltros.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnCancelar
        '
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancelar.Location = New System.Drawing.Point(349, 516)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(111, 33)
        Me.btnCancelar.TabIndex = 7
        Me.btnCancelar.Text = "&Cancelar"
        Me.btnCancelar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImprimir.Location = New System.Drawing.Point(166, 516)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(111, 33)
        Me.btnImprimir.TabIndex = 6
        Me.btnImprimir.Text = "&Imprimir"
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'VerAcceso2TableAdapter1
        '
        Me.VerAcceso2TableAdapter1.ClearBeforeFill = True
        '
        'VerAcceso2TableAdapter2
        '
        Me.VerAcceso2TableAdapter2.ClearBeforeFill = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnQuitarTodos)
        Me.GroupBox1.Controls.Add(Me.btnQuitarUno)
        Me.GroupBox1.Controls.Add(Me.btnAgregarTodos)
        Me.GroupBox1.Controls.Add(Me.btnAgregarUno)
        Me.GroupBox1.Controls.Add(Me.lbxAgregados)
        Me.GroupBox1.Controls.Add(Me.lbxPorAgregar)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 124)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(602, 386)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tipos de Gastos"
        '
        'btnQuitarTodos
        '
        Me.btnQuitarTodos.Location = New System.Drawing.Point(271, 314)
        Me.btnQuitarTodos.Name = "btnQuitarTodos"
        Me.btnQuitarTodos.Size = New System.Drawing.Size(60, 23)
        Me.btnQuitarTodos.TabIndex = 5
        Me.btnQuitarTodos.Text = "<<"
        Me.btnQuitarTodos.UseVisualStyleBackColor = True
        '
        'btnQuitarUno
        '
        Me.btnQuitarUno.Location = New System.Drawing.Point(271, 285)
        Me.btnQuitarUno.Name = "btnQuitarUno"
        Me.btnQuitarUno.Size = New System.Drawing.Size(60, 23)
        Me.btnQuitarUno.TabIndex = 4
        Me.btnQuitarUno.Text = "<"
        Me.btnQuitarUno.UseVisualStyleBackColor = True
        '
        'btnAgregarTodos
        '
        Me.btnAgregarTodos.Location = New System.Drawing.Point(271, 95)
        Me.btnAgregarTodos.Name = "btnAgregarTodos"
        Me.btnAgregarTodos.Size = New System.Drawing.Size(60, 23)
        Me.btnAgregarTodos.TabIndex = 3
        Me.btnAgregarTodos.Text = ">>"
        Me.btnAgregarTodos.UseVisualStyleBackColor = True
        '
        'btnAgregarUno
        '
        Me.btnAgregarUno.Location = New System.Drawing.Point(271, 66)
        Me.btnAgregarUno.Name = "btnAgregarUno"
        Me.btnAgregarUno.Size = New System.Drawing.Size(60, 23)
        Me.btnAgregarUno.TabIndex = 2
        Me.btnAgregarUno.Text = ">"
        Me.btnAgregarUno.UseVisualStyleBackColor = True
        '
        'lbxAgregados
        '
        Me.lbxAgregados.FormattingEnabled = True
        Me.lbxAgregados.ItemHeight = 16
        Me.lbxAgregados.Location = New System.Drawing.Point(337, 21)
        Me.lbxAgregados.Name = "lbxAgregados"
        Me.lbxAgregados.Size = New System.Drawing.Size(259, 356)
        Me.lbxAgregados.TabIndex = 1
        '
        'lbxPorAgregar
        '
        Me.lbxPorAgregar.FormattingEnabled = True
        Me.lbxPorAgregar.ItemHeight = 16
        Me.lbxPorAgregar.Location = New System.Drawing.Point(6, 21)
        Me.lbxPorAgregar.Name = "lbxPorAgregar"
        Me.lbxPorAgregar.Size = New System.Drawing.Size(259, 356)
        Me.lbxPorAgregar.TabIndex = 0
        '
        'lblCajera
        '
        Me.lblCajera.AutoSize = True
        Me.lblCajera.Location = New System.Drawing.Point(23, 30)
        Me.lblCajera.Name = "lblCajera"
        Me.lblCajera.Size = New System.Drawing.Size(79, 15)
        Me.lblCajera.TabIndex = 0
        Me.lblCajera.Text = "Cajero (a) :"
        '
        'cmbCajera
        '
        Me.cmbCajera.FormattingEnabled = True
        Me.cmbCajera.Location = New System.Drawing.Point(111, 27)
        Me.cmbCajera.Name = "cmbCajera"
        Me.cmbCajera.Size = New System.Drawing.Size(475, 23)
        Me.cmbCajera.TabIndex = 1
        '
        'lblFechaInicial
        '
        Me.lblFechaInicial.AutoSize = True
        Me.lblFechaInicial.Location = New System.Drawing.Point(5, 59)
        Me.lblFechaInicial.Name = "lblFechaInicial"
        Me.lblFechaInicial.Size = New System.Drawing.Size(97, 15)
        Me.lblFechaInicial.TabIndex = 2
        Me.lblFechaInicial.Text = "Fecha Inicial :"
        '
        'lblFechaFinal
        '
        Me.lblFechaFinal.AutoSize = True
        Me.lblFechaFinal.Location = New System.Drawing.Point(12, 86)
        Me.lblFechaFinal.Name = "lblFechaFinal"
        Me.lblFechaFinal.Size = New System.Drawing.Size(90, 15)
        Me.lblFechaFinal.TabIndex = 3
        Me.lblFechaFinal.Text = "Fecha Final :"
        '
        'dtpFechaInicial
        '
        Me.dtpFechaInicial.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicial.Location = New System.Drawing.Point(111, 56)
        Me.dtpFechaInicial.Name = "dtpFechaInicial"
        Me.dtpFechaInicial.Size = New System.Drawing.Size(110, 21)
        Me.dtpFechaInicial.TabIndex = 4
        '
        'dtpFechaFinal
        '
        Me.dtpFechaFinal.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaFinal.Location = New System.Drawing.Point(111, 83)
        Me.dtpFechaFinal.Name = "dtpFechaFinal"
        Me.dtpFechaFinal.Size = New System.Drawing.Size(110, 21)
        Me.dtpFechaFinal.TabIndex = 5
        '
        'gbxFiltros
        '
        Me.gbxFiltros.Controls.Add(Me.dtpFechaFinal)
        Me.gbxFiltros.Controls.Add(Me.dtpFechaInicial)
        Me.gbxFiltros.Controls.Add(Me.lblFechaFinal)
        Me.gbxFiltros.Controls.Add(Me.lblFechaInicial)
        Me.gbxFiltros.Controls.Add(Me.cmbCajera)
        Me.gbxFiltros.Controls.Add(Me.lblCajera)
        Me.gbxFiltros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxFiltros.Location = New System.Drawing.Point(12, 7)
        Me.gbxFiltros.Name = "gbxFiltros"
        Me.gbxFiltros.Size = New System.Drawing.Size(602, 111)
        Me.gbxFiltros.TabIndex = 1
        Me.gbxFiltros.TabStop = False
        '
        'FrmFiltroGastos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(628, 556)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.gbxFiltros)
        Me.Controls.Add(Me.btnImprimir)
        Me.Name = "FrmFiltroGastos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Filtro Entregas Globales"
        Me.GroupBox1.ResumeLayout(False)
        Me.gbxFiltros.ResumeLayout(False)
        Me.gbxFiltros.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCancelar As System.Windows.Forms.Button
    Friend WithEvents btnImprimir As System.Windows.Forms.Button
    Friend WithEvents VerAcceso2TableAdapter1 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents VerAcceso2TableAdapter2 As softvFacturacion.ProcedimientosArnoldo3TableAdapters.VerAcceso2TableAdapter
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnQuitarTodos As System.Windows.Forms.Button
    Friend WithEvents btnQuitarUno As System.Windows.Forms.Button
    Friend WithEvents btnAgregarTodos As System.Windows.Forms.Button
    Friend WithEvents btnAgregarUno As System.Windows.Forms.Button
    Friend WithEvents lbxAgregados As System.Windows.Forms.ListBox
    Friend WithEvents lbxPorAgregar As System.Windows.Forms.ListBox
    Friend WithEvents lblCajera As System.Windows.Forms.Label
    Friend WithEvents cmbCajera As System.Windows.Forms.ComboBox
    Friend WithEvents lblFechaInicial As System.Windows.Forms.Label
    Friend WithEvents lblFechaFinal As System.Windows.Forms.Label
    Friend WithEvents dtpFechaInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents gbxFiltros As System.Windows.Forms.GroupBox
End Class
