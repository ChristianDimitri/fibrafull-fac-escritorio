Imports System.Data.SqlClient


Public Class FrmDetalle_Conceptos_Facturar


    Private Sub FillToolStripButton_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub FillToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FillToolStripButton_Click_2(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FillToolStripButton_Click_3(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub FrmDetalle_Conceptos_Facturar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim con As New SqlConnection(MiConexion)
        con.Open()
        Me.Detalle_Conceptos_FacturarTableAdapter.Connection = con
        Me.Detalle_Conceptos_FacturarTableAdapter.Fill(Me.NewsoftvDataSet.Detalle_Conceptos_Facturar, GloContrato)
        con.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()

    End Sub

    Private Sub Borra(ByVal xClv_Session As Long)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            .CommandText = "Borra_PreCancelaciones  "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0

            Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = xClv_Session
            .Parameters.Add(prm)
            
            Dim i As Integer = comando.ExecuteNonQuery()
        End With
        CON.Close()
    End Sub


    Private Sub Inserta_DEtalle(ByVal xClv_Session As Long, ByVal xContrato As Long, ByVal xClv_Unicanet As Long, ByVal xClv_TipSer As Integer, ByVal xEsPaqAdic As Integer, ByVal xEsSerDig As Integer, ByVal xClv_Servicio As Integer, ByVal xDescripcion As String, ByVal xClvSer_Se_Inserta As Integer)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Dim comando As SqlClient.SqlCommand
        comando = New SqlClient.SqlCommand
        With comando
            .Connection = CON
            .CommandText = "Inserta_PreCancelaciones  "
            .CommandType = CommandType.StoredProcedure
            .CommandTimeout = 0

            Dim prm As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
            prm.Direction = ParameterDirection.Input
            prm.Value = xClv_Session
            .Parameters.Add(prm)

            Dim prm1 As New SqlParameter("@Contrato", SqlDbType.BigInt)
            prm1.Direction = ParameterDirection.Input
            prm1.Value = xContrato
            .Parameters.Add(prm1)

            Dim prm2 As New SqlParameter("@Clv_Unicanet", SqlDbType.BigInt)
            prm2.Direction = ParameterDirection.Input
            prm2.Value = xClv_Unicanet
            .Parameters.Add(prm2)

            Dim prm3 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
            prm3.Direction = ParameterDirection.Input
            prm3.Value = xClv_TipSer
            .Parameters.Add(prm3)

            Dim prm4 As New SqlParameter("@EsPaqAdic", SqlDbType.Bit)
            prm4.Direction = ParameterDirection.Input
            prm4.Value = xEsPaqAdic
            .Parameters.Add(prm4)


            Dim prm5 As New SqlParameter("@EsSerDig", SqlDbType.Bit)
            prm5.Direction = ParameterDirection.Input
            prm5.Value = xEsSerDig
            .Parameters.Add(prm5)

            Dim prm6 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
            prm6.Direction = ParameterDirection.Input
            prm6.Value = xClv_Servicio
            .Parameters.Add(prm6)

            Dim prm7 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 250)
            prm7.Direction = ParameterDirection.Input
            prm7.Value = xDescripcion
            .Parameters.Add(prm7)

            Dim prm8 As New SqlParameter("@ClvSer_Se_Inserta", SqlDbType.Int)
            prm8.Direction = ParameterDirection.Input
            prm8.Value = xClvSer_Se_Inserta
            .Parameters.Add(prm8)
            Dim i As Integer = comando.ExecuteNonQuery()
        End With
        CON.Close()
    End Sub


    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Borra(GloTelClv_Session)
        Dim i As Integer = 0
        For i = 0 To Me.DataGridView1.RowCount - 1
            If Me.DataGridView1.Item(0, i).Value = True Then
                Inserta_DEtalle(GloTelClv_Session, Me.DataGridView1.Item(2, i).Value, Me.DataGridView1.Item(3, i).Value, Me.DataGridView1.Item(4, i).Value, Me.DataGridView1.Item(5, i).Value, Me.DataGridView1.Item(6, i).Value, Me.DataGridView1.Item(7, i).Value, Me.DataGridView1.Item(1, i).Value, Me.DataGridView1.Item(8, i).Value)
            End If
        Next
        GloTiene_Cancelaciones = True
        Me.Close()

    End Sub
End Class