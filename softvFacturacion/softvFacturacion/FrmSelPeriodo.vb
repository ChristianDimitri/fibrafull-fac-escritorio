Imports System.Data.SqlClient

Public Class FrmSelPeriodo

    Private Sub FrmSelPeriodo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        GloBndControl = True
        If IsNumeric(Me.ComboBox1.SelectedValue) = False Then
            GloClv_Periodo_Num = 1
            GloClv_Periodo_Txt = " Primer Periodo"
            GloActPeriodo = 1
        Else
            GloActPeriodo = 1
            GloClv_Periodo_Num = Me.ComboBox1.SelectedValue
            GloClv_Periodo_Txt = Me.ComboBox1.Text
        End If
    End Sub

    Private Sub FrmSelPeriodo_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me)
        Dim CON As New SqlConnection(MiConexion)
        CON.Open()
        Me.MUESTRAPERIODOS_SeleccionarTableAdapter.Connection = CON
        Me.MUESTRAPERIODOS_SeleccionarTableAdapter.Fill(Me.NewsoftvDataSet2.MUESTRAPERIODOS_Seleccionar, 0)
        CON.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If IsNumeric(Me.ComboBox1.SelectedValue) = True Then
            GloActPeriodo = 1
            GloClv_Periodo_Num = Me.ComboBox1.SelectedValue
            GloClv_Periodo_Txt = Me.ComboBox1.Text
            GloBndControl = True
            Me.Close()
            FrmListadoPreliminar.Show()
        Else
            MsgBox("Seleccione el Periodo ", MsgBoxStyle.Information)
        End If
    End Sub
End Class