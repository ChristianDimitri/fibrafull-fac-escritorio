Imports System.Data.SqlClient
Imports System.Text
Public Class FrmVentaDeEquipo

    Private Sub Detalle(ByVal Contrato As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder
        strSQL.Append("EXEC DetalleVentaDeEquipo ")
        strSQL.Append(CStr(Contrato))

        Try
            conexion.Open()
            Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
            Dim dataTable As New DataTable
            Dim bindingSource As New BindingSource

            dataAdapter.Fill(dataTable)
            bindingSource.DataSource = dataTable
            Me.DataGridView1.DataSource = bindingSource
            conexion.Close()

            Me.DataGridView1.Columns(1).Visible = False
            Me.DataGridView1.Columns(2).Visible = False
            Me.DataGridView1.Columns(3).Visible = False
            Me.DataGridView1.Columns(4).Visible = False
            Me.DataGridView1.Columns(6).Visible = False
            Me.DataGridView1.Columns(7).Visible = False
            Me.DataGridView1.Columns(5).Width = 400
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Inserta(ByVal Clv_Session As Long, ByVal Contrato As Long, ByVal Clv_UnicaNet As Long, ByVal Clv_TipSer As Integer, ByVal Clv_Servicio As Integer, ByVal Descripcion As String, ByVal ClvSer_Se_Inserta As Integer, ByVal Clv_Equipo As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("InsertaPreVentaDeEquipo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Dim parametro2 As New SqlParameter("@Contrato", SqlDbType.BigInt)
        parametro2.Direction = ParameterDirection.Input
        parametro2.Value = Contrato
        comando.Parameters.Add(parametro2)

        Dim parametro3 As New SqlParameter("@Clv_UnicaNet", SqlDbType.BigInt)
        parametro3.Direction = ParameterDirection.Input
        parametro3.Value = Clv_UnicaNet
        comando.Parameters.Add(parametro3)

        Dim parametro4 As New SqlParameter("@Clv_TipSer", SqlDbType.Int)
        parametro4.Direction = ParameterDirection.Input
        parametro4.Value = Clv_TipSer
        comando.Parameters.Add(parametro4)

        Dim parametro5 As New SqlParameter("@Clv_Servicio", SqlDbType.Int)
        parametro5.Direction = ParameterDirection.Input
        parametro5.Value = Clv_Servicio
        comando.Parameters.Add(parametro5)

        Dim parametro6 As New SqlParameter("@Descripcion", SqlDbType.VarChar, 250)
        parametro6.Direction = ParameterDirection.Input
        parametro6.Value = Descripcion
        comando.Parameters.Add(parametro6)

        Dim parametro7 As New SqlParameter("@ClvSer_Se_Inserta", SqlDbType.Int)
        parametro7.Direction = ParameterDirection.Input
        parametro7.Value = ClvSer_Se_Inserta
        comando.Parameters.Add(parametro7)

        Dim parametro8 As New SqlParameter("@Clv_Equipo", SqlDbType.BigInt)
        parametro8.Direction = ParameterDirection.Input
        parametro8.Value = clv_equipo
        comando.Parameters.Add(parametro8)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

    End Sub

    Private Sub Borrar(ByVal Clv_Session As Long)
        Dim conexion As New SqlConnection(MiConexion)
        Dim comando As New SqlCommand("BorraPreVentaDeEquipo", conexion)
        comando.CommandType = CommandType.StoredProcedure

        Dim parametro As New SqlParameter("@Clv_Session", SqlDbType.BigInt)
        parametro.Direction = ParameterDirection.Input
        parametro.Value = Clv_Session
        comando.Parameters.Add(parametro)

        Try
            conexion.Open()
            comando.ExecuteNonQuery()
            conexion.Close()
        Catch ex As Exception
            conexion.Close()
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        Borrar(GloTelClv_Session)
        Dim i As Integer = 0
        For i = 0 To Me.DataGridView1.RowCount - 1
            If Me.DataGridView1.Item(0, i).Value = True Then
                Inserta(GloTelClv_Session, Me.DataGridView1.Item(1, i).Value, Me.DataGridView1.Item(2, i).Value, Me.DataGridView1.Item(3, i).Value, Me.DataGridView1.Item(4, i).Value, Me.DataGridView1.Item(5, i).Value, Me.DataGridView1.Item(6, i).Value, Me.DataGridView1.Item(7, i).Value)
            End If
        Next
        GloComproEquipo = True
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub FrmVentaDeEquipo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me)
        Detalle(GloContrato)
    End Sub
End Class